FROM gitpod/workspace-full

RUN sudo apt-get update \
    && sudo apt-get -y install qtbase5-dev libkf5config-dev qtdeclarative5-dev kirigami2-dev lldb \
    && rm -rf /var/apt/lists/*

RUN cd $(mktemp -d) \
    && curl -sSfO https://download.build2.org/0.13.0/build2-install-0.13.0.sh \
    && sh build2-install-0.13.0.sh --yes --trust yes --cxx clang++ \
    && (d=$(pwd); cd / && rm -rf $d)
